package com.example.testproject;

import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.example.testproject.databinding.ActivityMainBinding;
import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;


    private androidx.viewpager2.widget.ViewPager2 view_Pager;
    private TabLayout tabLayout;

    private String[] titles = new String[]{"Overview", "Hotels", "Transportation"};

    @Override
    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = com.example.testproject.databinding.ActivityMainBinding.inflate(getLayoutInflater());
        binding.toolbar.setTitle("");
        setContentView(binding.getRoot());


        init();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void init() {

        view_Pager = findViewById(R.id.view_pager);
        tabLayout = findViewById(com.example.testproject.R.id.tab_layout);
        view_Pager.setAdapter(new ViewPagerFragmentAdapter(this));
        // attaching tab mediator
        new com.google.android.material.tabs.TabLayoutMediator(tabLayout, view_Pager,
                (tab, position) -> tab.setText(titles[position])).attach();




        setSupportActionBar(binding.toolbar);
    }

    private class ViewPagerFragmentAdapter extends androidx.viewpager2.adapter.FragmentStateAdapter {

        public ViewPagerFragmentAdapter(@androidx.annotation.NonNull androidx.fragment.app.FragmentActivity fragmentActivity) {
            super(fragmentActivity);
        }

        @androidx.annotation.NonNull
        @Override
        public androidx.fragment.app.Fragment createFragment(int position) {
            switch (position) {
                case 0:
                    return new com.example.testproject.OverviewFragment();
                case 1:
                    return new com.example.testproject.HotelsFragment();
                case 2:
                    return new com.example.testproject.TransportationFragment();
            }
            return new com.example.testproject.OverviewFragment();
        }

        @Override
        public int getItemCount() {
            return titles.length;
        }
    }


    private class ViewPagerAdapter extends androidx.fragment.app.FragmentPagerAdapter {
        private java.util.List<androidx.fragment.app.Fragment> fragments = new java.util.ArrayList<>();
        private java.util.List<String> fragmentTitles = new java.util.ArrayList<>();
        public ViewPagerAdapter(@androidx.annotation.NonNull androidx.fragment.app.FragmentManager fm, int behavior) {
            super(fm, behavior);
        }
        public void addFragment(androidx.fragment.app.Fragment fragment, String title){
            fragments.add(fragment);
            fragmentTitles.add(title);
        }
        @androidx.annotation.NonNull
        @Override
        public androidx.fragment.app.Fragment getItem(int position) {
            return fragments.get(position);
        }
        @Override
        public int getCount() {
            return fragments.size();
        }
        //to setup title of the tab layout
        @androidx.annotation.Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }
    }

}